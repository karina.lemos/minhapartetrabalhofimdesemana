<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

?>

<?php get_header(); ?>
            <?php if (have_posts()) : ?>
	            <h1 class="page-title">
                    <?php _e( 'Resultados da pesquisa por:', 'petfeliztheme' ); ?>
                    <span><?php echo get_search_query(); ?></span>
				</h1>
			
			<?php
			while ( have_posts() ) :
                the_post();
                ?>
                <h3><a href="<?php the_permalink() ?>"> <?php the_title() ?> </a></h3>
                <?php 
            endwhile;
        else:
            //get_template_part('template-parts/content/content, 'none');
        
		endif;
		?>
<?php get_footer(); ?>
